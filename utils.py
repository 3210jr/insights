import numpy as np
from sklearn.preprocessing import LabelEncoder

symptoms_encoder = LabelEncoder()
symptoms_encoder.classes_ = np.load('./encoders/gp_symptoms_encoder.npy')


diagnoses_encoder = LabelEncoder()
diagnoses_encoder.classes_ = np.load('./encoders/gp_diagnoses_encoder.npy')


def encode_gender(gender):
    if gender == "female":
        return 0
    else:
        return 1


# format the ages into one hot encoded age groups
# 0 - 2, 3 - 5, 6 - 10, 11 - 15, 16 - 20, 21 - 25, 26 - 30, 31 - 40, 41 - 50, 51 - 60, 61+
def encode_ages(age):
    age = float(age)
    group_encoding = np.zeros(13)
    psychology_encoding = np.zeros(8)

    # ENCODE THE PATIENTS PSYCHOLOGICAL AGE - ACCORDING TO BEHAVIORAL PSYCHOLOGY
    if age <= 1.5:
        psychology_encoding[0] = 1

    elif age > 1.5 and age <= 3:
        psychology_encoding[1] = 1

    elif age > 3 and age <= 5:
        psychology_encoding[2] = 1

    elif age > 5 and age <= 12:
        psychology_encoding[3] = 1

    elif age > 12 and age <= 20:
        psychology_encoding[4] = 1

    elif age > 20 and age <= 30:
        psychology_encoding[5] = 1

    elif age > 40 and age <= 55:
        psychology_encoding[6] = 1

    else:
        psychology_encoding[7] = 1

    # ENCODE THE PATIENTS AGE - ACCORDING TO DECIDED OVERLAPPING AGE GROUPS
    if age <= 5:
        group_encoding[0] = 1

    if age >= 4 and age <= 10:
        group_encoding[1] = 1

    if age >= 8 and age <= 15:
        group_encoding[2] = 1

    if age >= 12 and age <= 19:
        group_encoding[3] = 1

    if age >= 17 and age <= 25:
        group_encoding[4] = 1

    if age >= 21 and age <= 29:
        group_encoding[5] = 1

    elif age >= 27 and age <= 35:
        group_encoding[6] = 1

    if age >= 31 and age <= 40:
        group_encoding[7] = 1

    if age >= 37 and age <= 45:
        group_encoding[8] = 1

    if age >= 41 and age <= 49:
        group_encoding[9] = 1

    if age >= 47 and age <= 55:
        group_encoding[10] = 1

    if age >= 51 and age <= 60:
        group_encoding[11] = 1

    if age >= 60:
        group_encoding[12] = 1

    return [group_encoding, psychology_encoding, np.array([age / 100])]


# encode the month as a distribution of time. i.e: March = [0,0.25,1,0.25,0,.......]
def encode_month(month):
    encoded_months = np.zeros(12)
    if (month == 1):
        # Jan
        encoded_months[0] = 1
        encoded_months[11] = 0.25
        encoded_months[1] = 0.25
        return encoded_months
    if (month == 12):
        # Dec
        encoded_months[11] = 1
        encoded_months[10] = 0.25
        encoded_months[0] = 0.25
        return encoded_months

    encoded_months[month - 1] = 1
    encoded_months[month] = 0.25
    encoded_months[month - 2] = 0.25
    return encoded_months

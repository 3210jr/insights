from flask import Flask, request, jsonify
from flask_cors import CORS
import numpy as np
from sklearn.preprocessing import LabelEncoder

from elsa_gp import general_diagnose
from diagnose_cervical_cancer import diagnose_cervical_cancer
from test_recommender import recommend_tests
from prescriptions_recommender import recommend_prescriptions


from utils import symptoms_encoder, diagnoses_encoder

app = Flask(__name__)
CORS(app)


@app.route('/')
def hello_mary():
    return 'Hello Elsa v1!'


@app.route('/predict_diagnoses', methods=['POST'])
def predict_diagnoses():
    params = request.json

    age = params.get("age", 40)
    gender = params.get("gender", 0)
    month = params.get("month", 6)
    symptoms = params.get("symptoms", [])

    # convert symptoms into lower case and account for non-string values
    symptoms = [str(x).lower() for x in symptoms]

    # get only the symptoms we have trained our algorithm on
    mask = np.isin(symptoms_encoder.classes_, symptoms)
    masked_symptoms = symptoms_encoder.classes_[mask]

    elsa = general_diagnose(masked_symptoms, month, age, gender)

    return jsonify({"Elsa": elsa})


@app.route('/recommended_tests', methods=['POST'])
def recommended_tests():
    params = request.json

    age = params.get("age", 40)
    gender = params.get("gender", 0)
    symptoms = params.get("symptoms", [])
    differential = params.get("differential", [])

    # convert symptoms into lower case and account for non-string values
    symptoms = [str(x).lower() for x in symptoms]

    test_suggestions = recommend_tests(age, gender, symptoms, differential)
    return jsonify({"tests": test_suggestions})


@app.route('/prescribe_medication', methods=['POST'])
def prescribe_medication():
    params = request.json

    age = params.get("age", 40)
    gender = params.get("gender", 0)
    symptoms = params.get("symptoms", [])
    diagnoses = params.get("diagnoses", [])

    # convert symptoms into lower case and account for non-string values
    symptoms = [str(x).lower() for x in symptoms]

    prescription_suggestions = recommend_prescriptions(
        age, gender, symptoms, diagnoses)
    print(prescription_suggestions)
    return jsonify({"prescriptions": prescription_suggestions})


@app.route('/cervical_cancer_diagnosis', methods=['POST'])
def caclulate_cervical_cancer_diagnosis():
    params = request.json
    diagnosis = diagnose_cervical_cancer(params)

    return jsonify({"Diagnoses": diagnosis})


if __name__ == "__main__":
    app.run()

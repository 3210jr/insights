import torch
from torch.nn import Linear, MSELoss
from torch.autograd import Variable
import torch.nn.functional as F
from sklearn.externals import joblib

# SVM models
# fbp
svm_fbp_ElliplicEnvelopModel = joblib.load(
    './models/cervical_cancer/svm_fbp_EllipticEnvelope.pkl')
svm_fbp_OneClassSVM = joblib.load(
    './models/cervical_cancer/svm_fbp_OneClassSVM.pkl')
svm_fbp_IsolationForestModel = joblib.load(
    './models/cervical_cancer/svm_fbp_IsolationForest.pkl')

# urea
svm_urea_ElliplicEnvelopModel = joblib.load(
    './models/cervical_cancer/svm_EllipticEnvelope.pkl')
svm_urea_OneClassSVM = joblib.load(
    './models/cervical_cancer/svm_OneClassSVM.pkl')
svm_urea_IsolationForestModel = joblib.load(
    './models/cervical_cancer/svm_IsolationForest.pkl')

# uric acid
svm_uric_acid_ElliplicEnvelopModel = joblib.load(
    './models/cervical_cancer/svm_uric-acid_EllipticEnvelope.pkl')
svm_uric_acid_OneClassSVM = joblib.load(
    './models/cervical_cancer/svm_uric-acid_OneClassSVM.pkl')
svm_uric_acid_IsolationForestModel = joblib.load(
    './models/cervical_cancer/svm_uric-acid_IsolationForest.pkl')


# constants
threshold = 0.0168

# define the autoencoder model


class CBCAutoEncoder(torch.nn.Module):
    def __init__(self, in_shape):
        super(CBCAutoEncoder, self).__init__()
        self.fc1 = Linear(in_shape, in_shape // 2)
        self.fc2 = Linear(in_shape // 2, in_shape)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x


# ['neutrophil', 'eos', 'lymphocyte', 'mcv', 'mch']
fbp_model = CBCAutoEncoder(5)
# ['neutrophil', 'eos', 'lymphocyte', 'mcv', 'mch', 'urea']
fbp_urea_model = CBCAutoEncoder(6)
# ['neutrophil', 'eos', 'lymphocyte', 'mcv', 'mch', 'urea', 'uric-acid']
fbp_uric_acid = CBCAutoEncoder(7)

critereon = MSELoss()


def diagnose_cervical_cancer(params):

    symptoms = params.get("symptoms")
    # extract the fpb results ['neutrophil', 'lymphocyte', 'mcv', 'eos', 'mch']
    neutrophil = params.get("neutrophil")
    lymphocyte = params.get("lymphocyte")
    mcv = params.get("mcv")
    eos = params.get("eos")
    mch = params.get("mch")

    # extract out the urinalysis results ['urea', 'uric-acid']
    urea = params.get("urea")
    uric_acid = params.get("uric-acid")

    # [neutrophil, eos, lymphocyte, mcv, mch, urea, uric_acid] = [2.2, 0.52, 1.15, 90.81, 28.49, 2.6, 300.53]

    ae_fbp_diagnosis = ae_fbp_diagnose(neutrophil, eos, lymphocyte, mcv, mch)

    svm_fbp_diagnosis = SVM_fbp_diagnosis(
        neutrophil, eos, lymphocyte, mcv, mch)

    if urea:
        ae_fbp_urea_diagnosis = ae_fbp_urea_diagnose(
            neutrophil, eos, lymphocyte, mcv, mch, urea)

        svm_urea_diagnosis = SVM_urea_diagnosis(
            neutrophil, eos, lymphocyte, mcv, mch, urea)
    else:
        ae_fbp_urea_diagnosis = 0
        svm_urea_diagnosis = 0

    if uric_acid:
        ae_fbp_urea_uric_acid_diagnosis = ae_fbp_urea_uric_acid_diagnose(
            neutrophil, eos, lymphocyte, mcv, mch, urea, uric_acid)

        svm_uric_acid_diagnosis = SVM_uric_acid_diagnosis(
            neutrophil, eos, lymphocyte, mcv, mch, uric_acid)
    else:
        ae_fbp_urea_uric_acid_diagnosis = 0
        svm_uric_acid_diagnosis = 0

    symptom_diagnosis = diagnose_by_symptoms(symptoms)

    return [ae_fbp_diagnosis, ae_fbp_urea_diagnosis, ae_fbp_urea_uric_acid_diagnosis, svm_fbp_diagnosis, svm_urea_diagnosis, svm_uric_acid_diagnosis, symptom_diagnosis]


symptoms_list = ["pelvic pain", 'spot bleeding', 'fever', 'blood in urine', 'foul smelling discharge', 'lower abdominal pain', 'chest pain', 'fatigue',
                 'constipation', 'lower back pain', 'difficulty in breathing', 'rectal bleeding', 'nausea', 'pain during sexual intercourse', 'difficulty micturation',
                 'post menopausal bleeding', 'yellowish vaginal discharge', 'palpitations', 'prolonged vaginal watery discharge', 'metromenorrhagia', 'abnormal bleeding', 'lower limbs swelling',
                 'vaginal discharge', 'vomiting', 'cough', 'occasional painful defecation', 'headache', 'frequent micturation', 'vaginal pain',
                 'waist pain', 'back pain', 'weight loss', 'prolonged vaginal bleeding', 'swelling', 'abdominal pain', 'difficulty passing stool', 'post coital bleeding', "vulva itching"]

symptom_freq = {"pelvic pain": 13, "spot bleeding": 2, "fever": 11, "blood in urine": 2, "foul smelling discharge": 365, "lower abdominal pain": 552, "chest pain": 51, "fatigue": 23, "constipation": 124, "lower back pain": 122, "difficulty in breathing": 35, "rectal bleeding": 5, "nausea": 9, "pain during sexual intercourse": 9, "difficulty micturation": 67, "post menopausal bleeding": 207, "yellowish vaginal discharge": 21, "palpitations": 13,
                "prolonged vaginal watery discharge": 312, "metromenorrhagia": 5, "abnormal bleeding": 91, "lower limbs swelling": 26, "vaginal discharge": 363, "vomiting": 25, "cough": 84, "occasional painful defecation": 4, "headache": 17, "frequent micturation": 50, "vaginal pain": 8, "waist pain": 31, "back pain": 145, "weight loss": 7, "prolonged vaginal bleeding": 672, "swelling": 21, "abdominal pain": 117, "difficulty passing stool": 48, "post coital bleeding": 88, "vulva itching": 3}

# Symptoms diagnosis


def diagnose_by_symptoms(symptoms=[]):
    risk = 0
    if len(symptoms) > 0:
        for symp in symptoms:
            try:
                risk = symptom_freq[symp] + risk
            except:
                pass

    risk = min(risk / 1024 * 100, 100) / 100
    if risk > 0.45:
        return 1
    else:
        return - 1


# SVM diagnoses
def SVM_fbp_diagnosis(neutrophil, eos, lymphocyte, mcv, mch):
    # ['neutrophil', 'lymphocyte', 'mcv', 'eos', 'mch']
    x = [neutrophil, eos, lymphocyte, mcv, mch]
    # combined classifiers
    one = svm_fbp_ElliplicEnvelopModel.predict(
        [[neutrophil, lymphocyte, mcv, eos, mch]])
    two = svm_fbp_OneClassSVM.predict(
        [[neutrophil, lymphocyte, mcv, eos, mch]])
    three = svm_fbp_IsolationForestModel.predict(
        [[neutrophil, lymphocyte, mcv, eos, mch]])

    result = -1
    if (one[0] + two[0] + three[0]) / 3 > 0.3:
        result = 1
    else:
        result = -1

    return result


def SVM_urea_diagnosis(neutrophil, eos, lymphocyte, mcv, mch, urea):
    # ['neutrophil', 'eos', 'lymphocyte', 'urea', 'mcv', 'mch']
    x = [neutrophil, eos, lymphocyte, urea, mcv, mch]

    # combined classifiers
    one = svm_urea_ElliplicEnvelopModel.predict([x])
    two = svm_urea_OneClassSVM.predict([x])
    three = svm_urea_IsolationForestModel.predict([x])

    result = -1
    if (one[0] + two[0] + three[0]) / 3 > 0.3:
        result = 1
    else:
        result = -1

    return result


def SVM_uric_acid_diagnosis(neutrophil, eos, lymphocyte, mcv, mch, uric_acid):
    # ['neutrophil', 'eos', 'lymphocyte', 'mcv', 'mch', 'uric-acid']
    x = [neutrophil, eos, lymphocyte, mcv, mch, uric_acid]

    # combined classifiers
    one = svm_uric_acid_ElliplicEnvelopModel.predict([x])
    two = svm_uric_acid_OneClassSVM.predict([x])
    three = svm_uric_acid_IsolationForestModel.predict([x])

    result = -1
    if (one[0] + two[0] + three[0]) / 3 > 0.3:
        result = 1
    else:
        result = -1

    return result


# autoencoder diagnoses
def ae_fbp_diagnose(neutrophil, eos, lymphocyte, mcv, mch):
    threshold = 0.06
    fbp_model.load_state_dict(torch.load('./models/cervical_cancer/ae_fbp.pt'))
    fbp_model.eval()
    x = fbp_standardize(neutrophil, eos, lymphocyte, mcv, mch)
    y_pred = fbp_model(Variable(torch.Tensor(x)))
    loss = critereon(y_pred, Variable(torch.Tensor(x)))

    diagnosis = -1

    if loss.data[0] >= threshold:
        diagnosis = -1
    else:
        diagnosis = 1

    return diagnosis


def ae_fbp_urea_diagnose(neutrophil, eos, lymphocyte, mcv, mch, urea):
    threshold = 0.02443
    fbp_urea_model.load_state_dict(torch.load(
        './models/cervical_cancer/ae_urea_fbp.pt'))
    fbp_urea_model.eval()
    x = fbp_standardize(neutrophil, eos, lymphocyte, mcv,
                        mch) + urinalysis_standardize(urea)

    y_pred = fbp_urea_model(Variable(torch.Tensor(x)))
    loss = critereon(y_pred, Variable(torch.Tensor(x)))

    diagnosis = -1

    if loss.data[0] >= threshold:
        diagnosis = -1
    else:
        diagnosis = 1

    return diagnosis


def ae_fbp_urea_uric_acid_diagnose(neutrophil, eos, lymphocyte, mcv, mch, urea, uric_acid):
    threshold = 0.022
    fbp_uric_acid.load_state_dict(torch.load(
        './models/cervical_cancer/ae_uric-acid_fbp.pt'))
    fbp_uric_acid.eval()
    x = fbp_standardize(neutrophil, eos, lymphocyte, mcv, mch) + \
        urinalysis_standardize(urea=urea, uric_acid=uric_acid)

    print("HERE: ", x)

    y_pred = fbp_uric_acid(Variable(torch.Tensor(x)))
    loss = critereon(y_pred, Variable(torch.Tensor(x)))

    diagnosis = -1

    if loss.data[0] >= threshold:
        diagnosis = -1
    else:
        diagnosis = 1

    return diagnosis


# standardize functions
def fbp_standardize(neutrophil, eos, lymphocyte, mcv, mch):
    neutrophil = min(1, neutrophil / 100)
    lymphocyte = min(1, lymphocyte / 100)
    mcv = min(1, mcv / 100)
    eos = min(1, eos / 100)
    mch = min(1, mch / 100)

    return [neutrophil, eos, lymphocyte, mcv, mch]


def urinalysis_standardize(urea=None, uric_acid=None):
    if urea and uric_acid:
        urea = min(1, urea / 100)
        uric_acid = min(1, uric_acid / 1000)

        print(urea, uric_acid)

        return [urea, uric_acid]

    if urea:
        urea = min(1, urea / 100)
        return [urea]

    if uric_acid:
        uric_acid = min(1, uric_acid / 1000)
        return [uric_acid]

# import libraries
import torch
from torch import Tensor
import torch.nn.functional as F
from torch.autograd import Variable
from torch.optim import SGD, Adam, RMSprop, Adagrad
from torch.nn import Linear, MSELoss, Module, Dropout, BCELoss, BCEWithLogitsLoss
import numpy as np
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
import math


from utils import encode_month, encode_ages, encode_gender

symptoms_encoder = LabelEncoder()
symptoms_encoder.classes_ = np.load('./encoders/gp_symptoms_encoder.npy')

diagnoses_encoder = LabelEncoder()
diagnoses_encoder.classes_ = np.load('./encoders/gp_diagnoses_encoder.npy')

# create the common zero length lists for hot encoding
symptom_zeros = np.zeros(len(symptoms_encoder.classes_))
diagnoses_zeros = np.zeros(len(diagnoses_encoder.classes_))



# constants THESE MUST BE CHANGED WITH EVERY UPDATE
input_dim = 173
out_dim = 187



# produce an array with one of k encodings of symptoms
def one_hot_symptoms(symptoms_list = []):
    if len(symptoms_list) > 0:
        encoded_symptoms = symptoms_encoder.transform(symptoms_list)
        encoded = np.copy(symptom_zeros)
        for ix in np.nditer(encoded_symptoms):
            encoded[ix] = 1
        return np.array(encoded)
    else:
        return np.copy(symptom_zeros)


# produce an array with one of k encodings of symptoms
def one_hot_diagnoses(diagnoses_list):
    encoded_diangoses = diagnoses_encoder.transform(diagnoses_list)
    encoded = np.copy(diagnoses_zeros)
    for ix in np.nditer(encoded_diangoses):
        encoded[ix] = 1
    return np.array(encoded)


# define the model
class DiagnosisClassifier(Module):
    def __init__(self):
        super(DiagnosisClassifier, self).__init__()
        self.ae_fc1 = Linear(input_dim, 180)
        self.fc1 = Linear(input_dim, 250)
        self.dropout = Dropout(p=0.5)
        self.ae_fc2 = Linear(180, 180)
#         self.fc2 = Linear(200, 200)
        self.fc3 = Linear(250, 250)
        self.fc4 = Linear(250, out_dim)
        
    def forward(self, x):
        x = F.leaky_relu(self.fc1(x))
        x = self.dropout(x)
        x = F.leaky_relu(self.fc3(x))
        x = self.dropout(x)
        x = F.sigmoid(self.fc4(x))
        return x
    

diag_model = DiagnosisClassifier()
optimizer = Adam(diag_model.parameters())
critereon = BCELoss()



diag_model.eval()

diag_model.load_state_dict(torch.load('./models/gp_diagnoses_state.pt'))


def general_diagnose(symptoms = [], month = 6, age = 35, gender = "female"):
    encoded_pt = np.array([])
    encoded_pt = np.insert(encoded_pt, 0, one_hot_symptoms(symptoms))
    encoded_pt = np.insert(encoded_pt, 0, encode_month(month))
    encoded_pt = np.insert(encoded_pt, 0, np.concatenate(encode_ages(age)))
    encoded_pt = np.insert(encoded_pt, 0, encode_gender(gender))
    
    
    predictions = diag_model(Variable(Tensor(encoded_pt))).data.numpy()
    top_5_predictions = predictions[predictions.argsort()[-5:][::-1]]
    decoded_diagnoses = diagnoses_encoder.inverse_transform(predictions.argsort()[-5:][::-1])
    print(top_5_predictions)
    print(decoded_diagnoses)

    results = []

    count = 0
    for ix in top_5_predictions:
        result = {
            "diagnosis": decoded_diagnoses[count],
            "confidence": round(float(ix), 4)
        }
        results.append(result)
        count = count + 1

    return results #diagnoses_encoder.inverse_transform(predictions.argsort()[-5:][::-1])
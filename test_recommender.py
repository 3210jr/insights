# import libraries
import torch
from torch import Tensor
import torch.nn.functional as F
from torch.autograd import Variable
from torch.optim import SGD, Adam, RMSprop, Adagrad
from torch.nn import Linear, MSELoss, Module, Dropout, BCELoss, BCEWithLogitsLoss
import numpy as np
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
import math


tests_encoder = LabelEncoder().fit(
    np.load("./models/test_recommender/tests_encoder.npy"))
symptoms_encoder = LabelEncoder().fit(
    np.load("./models/test_recommender/symptoms_encoder.npy"))
diagnoses_encoder = LabelEncoder().fit(
    np.load("./models/test_recommender/diagnoses_encoder.npy"))


symptom_zeros = np.zeros(len(symptoms_encoder.classes_))
diagnoses_zeros = np.zeros(len(diagnoses_encoder.classes_))
test_zeros = np.zeros(len(tests_encoder.classes_))


input_dim = 312
out_dim = 55


# produce an array with one of k encodings of tests
def one_hot_tests(tests_list):
    encoded_tests = tests_encoder.transform(tests_list)
#     return encoded_tests
    encoded = np.copy(test_zeros)
    for ix in np.nditer(encoded_tests):
        encoded[ix] = 1
    return np.array(encoded)

# produce an array with one of k encodings of symptoms


def one_hot_symptoms(symptoms_list=[]):
    if len(symptoms_list) > 0:
        encoded_symptoms = symptoms_encoder.transform(symptoms_list)
        encoded = np.copy(symptom_zeros)
        for ix in np.nditer(encoded_symptoms):
            encoded[ix] = 1
        return np.array(encoded)
    else:
        return np.copy(symptom_zeros)

# produce an array with one of k encodings of symptoms


def one_hot_diagnoses(diagnoses_list):
    encoded_diangoses = diagnoses_encoder.transform(diagnoses_list)
    encoded = np.copy(diagnoses_zeros)
    for ix in np.nditer(encoded_diangoses):
        encoded[ix] = 1
    return np.array(encoded)


def decode_test(encoded_test, threshold=0.5):
    # indexes = np.nonzero(encoded_test >= 0.5)[0]
    # print(tests_encoder.classes_)
    indexes = []
    tests = []
    for ix, num in enumerate(encoded_test):
        if (num >= threshold):
            indexes.append(ix)
            tests.append(tests_encoder.classes_[ix])
    return tests
    # return tests_encoder.inverse_transform(indexes)


def decode_diagnosis(encoded_diagnosis=[], threshold=0.5):
    indexes = np.nonzero(encoded_diagnosis >= 0.5)[0]
    return diagnoses_encoder.inverse_transform(indexes)


def decode_symptoms(encoded_symptoms, threshold=0.5):
    indexes = np.nonzero(encoded_symptoms >= 0.5)[0]
    return symptoms_encoder.inverse_transform(indexes)


def decode_patient(patient):
    symptoms = decode_symptoms(patient[:len(symptom_zeros)])
    diagnoses = decode_diagnosis(
        patient[len(symptom_zeros):len(symptom_zeros) + len(diagnoses_zeros)])
    age = patient[len(symptom_zeros)+len(diagnoses_zeros)
                      :len(symptom_zeros)+len(diagnoses_zeros)+1][0] * 100
    gender = patient[len(symptom_zeros)+len(diagnoses_zeros)+1]
    return [symptoms, diagnoses, age, gender]


# define the model
class RecommendationModel(Module):
    def __init__(self):
        super(RecommendationModel, self).__init__()
        self.fc1 = Linear(input_dim, 350)
        self.dropout = Dropout(p=0.5)
        self.fc2 = Linear(350, 350)
        self.fc3 = Linear(350, out_dim)

    def forward(self, x):
        x = F.leaky_relu(self.fc1(x))
        x = self.dropout(x)
        x = F.leaky_relu(self.fc2(x))
        x = self.dropout(x)
        x = F.sigmoid(self.fc3(x))
        return x


model = RecommendationModel()
optimizer = Adam(model.parameters())
critereon = BCELoss()

model.eval()
model.load_state_dict(torch.load(
    './models/test_recommender/gp_test_recommender_state.pt'))


def recommend_tests(age, gender, symptoms, differential):
    age = age / 100
    gender = gender

    # get only the symptoms we have trained our algorithm on
    mask = np.isin(symptoms_encoder.classes_, symptoms)
    symptoms = symptoms_encoder.classes_[mask]

    _symptoms = one_hot_symptoms(symptoms)
    _diagnoses = one_hot_diagnoses(differential)
    trt = model(Variable(Tensor(np.concatenate(
        (_symptoms, _diagnoses, [age], [gender])))))
    return decode_test(trt, threshold=0.4)
